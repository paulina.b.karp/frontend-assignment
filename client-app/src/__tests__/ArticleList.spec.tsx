import React from 'react';
import renderer from 'react-test-renderer';
import ArticleList from '../components/ArticleList/ArticleList';

it('ArticleList renders list correctly', () => {
  const mockArticles = [{
    id: 'id',
    date: 'date',
    category: 'category',
    preamble: 'preamble',
    title: 'title',
    image: ''
  }, {
    id: 'id2',
    date: 'date',
    category: 'category',
    preamble: 'preamble',
    title: 'title',
    image: ''
  }]
  const tree = renderer
      .create(<ArticleList articles={mockArticles}/>)
      .toJSON();
  expect(tree).toMatchSnapshot();
});

it('ArticleList renders empty state correctly', () => {
  const tree = renderer
      .create(<ArticleList articles={[]}/>)
      .toJSON();
  expect(tree).toMatchSnapshot();
});