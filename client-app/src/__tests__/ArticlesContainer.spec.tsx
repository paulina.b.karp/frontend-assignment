import React from 'react';
import renderer from 'react-test-renderer';
import ArticlesContainer from '../components/ArticlesContainer/ArticlesContainer';

it('ArticlesContainer renders error page', () => {
  const tree = renderer
      .create(<ArticlesContainer />)
      .toJSON();
  expect(tree).toMatchSnapshot();
});