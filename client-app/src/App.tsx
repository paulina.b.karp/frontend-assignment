import React from 'react';
import ArticlesContainer from './components/ArticlesContainer/ArticlesContainer'
import ErrorBoundary from "./components/ErrorBoundary/ErrorBoundary";

const App = () => <ErrorBoundary><ArticlesContainer/></ErrorBoundary>;

export default App;
