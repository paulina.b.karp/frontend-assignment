export interface ArticleFiltersProps {
  filters: string[]
  onFilteringSelected: (filters: string[]) => void
}