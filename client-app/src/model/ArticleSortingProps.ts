import {SortingType} from "../enums/SortingType";

export interface ArticlesSortingProps {
  onSortingSelected: (sort: SortingType) => void
}