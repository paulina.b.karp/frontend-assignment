import {SortingType} from "../enums/SortingType";

export interface ArticlesSortingState {
  sort: SortingType
}