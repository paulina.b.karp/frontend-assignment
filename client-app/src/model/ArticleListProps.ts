import {Article} from './Article';

export interface ArticleListProps {
  articles: Article[]
}