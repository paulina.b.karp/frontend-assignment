import {Article} from "./Article";
import {SortingType} from "../enums/SortingType";

export interface ArticlesContainerState {
  articles: Article[];
  filteredArticles: Article[];
  filters: string[];
  sorting: SortingType;
  error: boolean;
}