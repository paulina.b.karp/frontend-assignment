import {Article} from "./Article";

export interface ArticleItemProps {
  article: Article
}