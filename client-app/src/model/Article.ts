export interface Article {
  id: string;
  date: string;
  category: string;
  title: string;
  preamble: string;
  image: string;
}