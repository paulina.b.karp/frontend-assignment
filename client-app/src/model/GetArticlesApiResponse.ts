import {Article} from './Article';

export interface GetArticlesApiResponse {
  articles: Article[]
}