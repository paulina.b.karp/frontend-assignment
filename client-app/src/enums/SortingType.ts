export enum SortingType {
  'DESC' = -1,
  'ASC' = 1
}