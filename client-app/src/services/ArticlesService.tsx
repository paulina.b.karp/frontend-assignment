import {GetArticlesApiResponse} from '../model/GetArticlesApiResponse';

const API_PATH = 'http://localhost:6010/articles';

export function getSportArticles(): Promise<GetArticlesApiResponse> {
  return fetch(`${API_PATH}/sports`).then(response => response.json())
}

export function getFashionArticles(): Promise<GetArticlesApiResponse> {
  return fetch(`${API_PATH}/fashion`).then(response => response.json())
}