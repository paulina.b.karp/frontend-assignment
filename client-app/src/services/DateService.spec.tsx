import { datesComparator } from './DateService';

test('datesComparator compares dates in ascending order', () => {
  const sortOrder = datesComparator('1. oktober 2018', '8. november 2018', 1);

  expect(sortOrder).toEqual(-1);
});

test('datesComparator compares dates in descending order', () => {
  const sortOrder = datesComparator('1. oktober 2018', '8. november 2018', -1);

  expect(sortOrder).toEqual(1);
});
