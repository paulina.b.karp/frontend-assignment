import moment from 'moment'
import 'moment/locale/nn'
import {SortingType} from "../enums/SortingType";

const LOCALE = 'nn'
const DATE_FORMAT = 'LL'

moment.locale(LOCALE);

export function datesComparator(date1: string, date2: string, sortingType: SortingType): number {
  const isNewer = getTime(date1) < getTime(date2)
  return isNewer ? -sortingType : sortingType
}

function getTime(date: string): number {
  return moment(date, DATE_FORMAT).unix()
}