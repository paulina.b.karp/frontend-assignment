import React, {Component} from 'react';
import {getSportArticles, getFashionArticles} from '../../services/ArticlesService';
import ArticleList from '../ArticleList/ArticleList';
import ArticleFilters from '../ArticleFilters/ArticleFilters';
import {ArticlesContainerState} from '../../model/ArticlesContainerState';
import ArticleSorting from '../ArticleSorting/ArticleSorting';
import {SortingType} from '../../enums/SortingType';
import {datesComparator} from '../../services/DateService';
import {ErrorMessage} from '../ErrorMessage/ErrorMessage';

export default class ArticlesContainer extends Component<{}, ArticlesContainerState> {

  constructor(props: unknown) {
    super(props);
    this.state = {
      articles: [],
      filteredArticles: [],
      filters: [],
      sorting: SortingType.DESC,
      error: false
    }
  }

  componentDidMount(): void {
    this.fetchData()
  }

  fetchData = async () => {
    try {
      const [sportsData, fashionData] = await Promise.all([
        getSportArticles(),
        getFashionArticles(),
      ]);
      const articles = [...sportsData.articles, ...fashionData.articles]
      const filters = Array.from(new Set(articles.map(article => article.category)))
      this.setState({articles, filteredArticles: articles, filters, error: false})
      this.onSortingSelected(SortingType.DESC)
    } catch (e) {
      this.setState({error: true})
    }
  }

  onFilteringSelected = (filters: string[]) => {
    let filteredArticles = [...this.state.articles]
    if (filters.length) {
      filteredArticles = this.state.articles.filter(({category}) => filters.includes(category))
    }
    this.setState({filteredArticles}, () => this.onSortingSelected(this.state.sorting))
  }

  onSortingSelected = (sorting: SortingType) => {
    const filteredArticles = [...this.state.filteredArticles]
        .sort((a, b) => datesComparator(a.date, b.date, sorting))
    this.setState({filteredArticles, sorting})
  }

  refresh = () => {
    this.fetchData()
  }

  render() {
    if (!this.state.error) {
      return (
          <div className="container font-light flex flex-grow flex-col sm:flex-row mx-auto pt-6 pb-8">
            <div className="sm:w-2/12 w-full mx-1 sm:mx-2 sm:mt-12">
              <ArticleFilters filters={this.state.filters} onFilteringSelected={this.onFilteringSelected} />
            </div>
            <div className="sm:w-10/12 w-full mx-1 sm:mx-2">
              <ArticleSorting onSortingSelected={this.onSortingSelected} />
              <ArticleList articles={this.state.filteredArticles} />
            </div>
          </div>
      )
    } else {
      return <ErrorMessage onRefresh={() => this.refresh()} />
    }
  }
}