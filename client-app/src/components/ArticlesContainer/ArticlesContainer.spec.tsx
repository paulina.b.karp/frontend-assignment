import * as React from 'react';
import {act} from 'react-dom/test-utils';
import {render, unmountComponentAtNode} from 'react-dom';
import ArticlesContainer from './ArticlesContainer';

describe('ArticlesContainer', function () {
  
  let container;

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });
  
  it('should not display error message on init', function () {
    const container = document.createElement('div');
    document.body.appendChild(container);
    act(() => {
      render(<ArticlesContainer />, container);
    })

    const list = container.querySelector('ArticleList');
    expect(list).toBeDefined()
  });

  it('should display error message when request failed', function () {
    const container = document.createElement('div');
    document.body.appendChild(container);

    global.fetch = jest.fn().mockImplementation(() => Promise.resolve({
      json: () => Promise.reject()
    }));

    act(() => {
      render(<ArticlesContainer />, container);
    })

    const errorMessage = container.querySelector('ErrorMessage');
    expect(errorMessage).toBeDefined()
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });
});
