import React, {Component} from 'react';
import {ErrorBoundaryState} from '../../model/ErrorBoundaryState';

export default class ErrorBoundary extends Component<{}, ErrorBoundaryState> {
  constructor(props: {}) {
    super(props);
    this.state = {hasError: false};
  }

  componentDidCatch(error: Error) {
    this.setState({hasError: true});
    console.error(error)
  }

  render() {
    if (this.state.hasError) {
      return (
          <div>
            <h1>Something went wrong. Please try again later</h1>;
          </div>
      )
    }
    return this.props.children;
  }
}