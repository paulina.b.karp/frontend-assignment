import React, {Component, Fragment} from 'react';
import {ArticleFiltersProps} from '../../model/ArticleFiltersProps';
import {ArticleFiltersState} from '../../model/ArticleFiltersState';

export default class ArticleFilters extends Component<ArticleFiltersProps, ArticleFiltersState> {

  constructor(props: ArticleFiltersProps) {
    super(props)
    this.state = {
      appliedFilters: []
    }
  }

  handleChange(e): void {
    const filterName = e.target.name;
    const isChecked = e.target.checked;
    if (isChecked) {
      const appliedFilters = [...this.state.appliedFilters, filterName]
      this.setState({appliedFilters}, this.filteringSelected)
    } else {
      const appliedFilters = this.state.appliedFilters.filter(filter => filter !== filterName)
      this.setState({appliedFilters}, this.filteringSelected)
    }
  }

  filteringSelected = (): void => {
    this.props.onFilteringSelected(this.state.appliedFilters)
  }

  render() {
    return (
        <Fragment>
          <h2 className="mb-2 sm:mb-3 underline text-lg">Data sources</h2>
          <div className="flex flex-row sm:flex-col">
            {this.props.filters.map(filter => (
                <div key={filter} className="sm:mt-2 mr-5">
                  <input
                      id={filter}
                      className="align-middle"
                      type="checkbox"
                      name={filter}
                      checked={this.state.appliedFilters.includes(filter)}
                      onChange={(e) => this.handleChange(e)} />
                  <label className="ml-2 capitalize" htmlFor={filter}>{filter}</label>
                </div>)
            )}
          </div>
        </Fragment>
    )
  }
}