import * as React from 'react';
import {act} from 'react-dom/test-utils';
import ArticleList from './ArticleList';
import {render, unmountComponentAtNode} from 'react-dom';

describe('ArticleList', function () {
  let container;

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  })

  it('should display empty state', function () {
    act(() => {
      render(<ArticleList articles={[]} />, container);
    })
    const div = container.querySelector('div');
    expect(div.id).toBe('empty-state')
  });

  it('should display articles', function () {
    const mockArticles = [{
      id: 'id',
      date: 'date',
      category: 'category',
      preamble: 'preamble',
      title: 'title',
      image: ''
    }, {
      id: 'id2',
      date: 'date',
      category: 'category',
      preamble: 'preamble',
      title: 'title',
      image: ''
    }]
    act(() => {
      render(<ArticleList articles={mockArticles} />, container);
    })
    const items = container.querySelectorAll('li');
    expect(items.length).toBe(2)
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });
});