import React from 'react';
import ArticleItem from '../ArticleItem/ArticleItem';
import {ArticleListProps} from '../../model/ArticleListProps';
import {Article} from '../../model/Article'

export default function ArticleList(props: ArticleListProps) {

  if (props.articles.length) {
    return (
        <ul className="mt-12">{
          props.articles.map((article: Article) => (
              <ArticleItem key={article.id} article={article} />)
          )
        }</ul>
    )
  } else {
    return (
        <div id="empty-state" className="mt-12" >
          There are no articles to display
        </div>
    )
  }
}