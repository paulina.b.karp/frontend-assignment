import * as React from 'react';
import {act} from 'react-dom/test-utils';
import {render, unmountComponentAtNode} from 'react-dom';
import ArticleSorting from './ArticleSorting';

describe('ArticleSorting', function () {

  let container;

  it('should display ascending icon on init', function () {
    container = document.createElement('div');
    document.body.appendChild(container);

    act(() => {
      render(<ArticleSorting onSortingSelected={jest.fn()} />, container);
    })

    const svg = container.querySelector('svg');
    expect(svg.id).toBe('icon-ascending')
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });
});