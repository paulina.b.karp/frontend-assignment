import React, {Component} from 'react';
import {ArticlesSortingState} from '../../model/ArticleSortingState';
import {ArticlesSortingProps} from '../../model/ArticleSortingProps';
import {SortingType} from '../../enums/SortingType';

export default class ArticleSorting extends Component<ArticlesSortingProps, ArticlesSortingState> {

  constructor(props: ArticlesSortingProps) {
    super(props)
    this.state = {
      sort: SortingType.ASC
    }
  }

  onSortingSelected = (): void => {
    const sort = this.state.sort === SortingType.ASC ? SortingType.DESC : SortingType.ASC
    this.setState({sort})
    this.props.onSortingSelected(this.state.sort)
  }

  render() {
    return (
        <button role="button" className="float-right font-light" onClick={() => this.onSortingSelected()}>
          Sort by date
          {this.state.sort === SortingType.ASC && (
              <svg id="icon-ascending" className="h-6 w-6 ml-2 inline font-light" xmlns="http://www.w3.org/2000/svg"
                   fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                      d="M3 4h13M3 8h9m-9 4h6m4 0l4-4m0 0l4 4m-4-4v12" />
              </svg>
          )}
          {this.state.sort === SortingType.DESC && (
              <svg id="icon-descending" className="h-6 w-6 ml-2 inline font-light" xmlns="http://www.w3.org/2000/svg"
                   fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                      d="M3 4h13M3 8h9m-9 4h9m5-4v12m0 0l-4-4m4 4l4-4" />
              </svg>
          )}
        </button>
    )
  }
}