import React from 'react';
import {ErrorMessageProps} from '../../model/ErrorMessageProps';

export function ErrorMessage(props: ErrorMessageProps) {

  return (
      <div className="p-20 text-center">
        There was an error while fetching the data. Please try again.
        <button role="button" className="mt-5 m-auto block bg-transparent text-blue-900 font-semibold hover:text-black py-2 px-4 border border-blue-900 rounded"
                onClick={props.onRefresh}>Refresh</button>
      </div>
  )
}