import * as React from 'react';
import {act} from 'react-dom/test-utils';
import ArticleItem from './ArticleItem';
import { render, unmountComponentAtNode } from 'react-dom';

describe('ArticleItem', function () {
  let container;

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  })

  it('should display article image', function () {
    const mockArticle = {
      title: 'title',
      preamble: 'preamble',
      date: 'date',
      id: 'id',
      category: 'sports',
      image: 'some-image.png'
    }
    act(() => {
      render(<ArticleItem article={mockArticle} />, container);
    })
    const img = container.querySelector('img');
    expect(img.src).toBe('http://localhost/some-image.png')
  });

  it('should display default image', function () {
    const mockArticle = {
      title: 'title',
      preamble: 'preamble',
      date: 'date',
      id: 'id',
      category: 'sports',
      image: ''
    }
    act(() => {
      render(<ArticleItem article={mockArticle} />, container);
    })
    const img = container.querySelector('img');
    expect(img.src).toBe('http://localhost/images/default.png')
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });
});