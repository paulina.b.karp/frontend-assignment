import React from 'react';
import {ArticleItemProps} from '../../model/ArticleItemProps';

export default function ArticleItem(props: ArticleItemProps) {

  return (
      <li className="container flex my-5">
        <img
            className="w-1/3 sm:w-2/12 inline-block pr-3 object-contain object-top"
            alt="Presentation of article"
            src={props.article.image || '/images/default.png'} />
        <div className="w-2/3 sm:w-10/12 inline-block">
          <h2 className="inline-block text-l sm:text-xl">{props.article.title}</h2>
          <span className="float-right text-m">
            {props.article.date}
          </span>
          <div className="hidden sm:block w-full mt-5 mr-10">
            {props.article.preamble}
          </div>
        </div>
      </li>
  )
}