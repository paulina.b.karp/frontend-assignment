# Articles List Client App

Client-app displaying list of articles with an ability to filter them and sort by date.

## Technical notes
App is implemented using React 17 using Node in version 14, tested with Jest. 
Styles are built with tailwind.

## Run instruction
Requirements:
- node v14

Run instructions (from root directory)
```
cd client-app
npm install
npm start
```

## Potential improvements
There are a few areas worth improving in the future:
- adding list loader 
- adding list pagination when there is more data
- additional unit tests, e2e tests
- additional service handling API requests in case of more complicated logic (e.g. authentication)
- more personalised styles, updates to mobile view